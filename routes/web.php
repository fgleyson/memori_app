<?php   

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CreateController;

Route::get('/', [AuthController::class, 'dashboard'])->name('admin');




Route::get('/login', [AuthController::class, 'showLoginForm'])->name('formlogin');
Route::get('/home', [AuthController::class, 'login'])->name('home');
// Rota que cai o login e senha pra saber se ta certo ou errado
Route::post('/home', [AuthController::class, 'login'])->name('home');


Route::get('/logout', [AuthController::class, 'logout'])->name('logout');




Route::get('/form', [CreateController::class, 'form'])->name('form');

Route::post('/create', [CreateController::class, 'create'])->name('create');



Route::get('/listall', [CreateController::class, 'show'])->name('listall');


Route::get('/formupdate', [CreateController::class, 'formupdate'])->name('formupdate');

Route::get('/update', [CreateController::class, 'update'])->name('update');
