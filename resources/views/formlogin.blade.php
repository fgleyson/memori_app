<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <title>Memori</title>
       
       <!--  Fonte do Google -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">

       <!--  CSS Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
       
       <!--  CSS/JS da aplicação -->
        <link rel="stylesheet" href="/css/styles.css">
        <script src="/js/scripts.js"></script>
    </head>

    <body>

    <div class="camp-form">
        <form class="form-signin" method="post" action="{{ route('home') }}">
        @csrf

            @if ($errors->all())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <label for="email" class="sr-only">Login</label>
            <input type="email" name="email" id="email" class="form-control" value="eduardomariano@nucleus.eti.br" required>

            <label for="password" class="sr-only">Senha</label>
            <input type="password" name="senha"  id="password" class="form-control" value="secret" placeholder="Digite sua senha">

            <button class="btn btn-primary" type="submit">Login</button>
            {{-- <button class="btn btn-primary" type="submit"><a href="{{ route('create') }}">Criar Conta</a></button> --}}

        </form>
    </div>          
        <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
    </body>
</html>
