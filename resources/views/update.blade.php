@extends('layouts.main')
@section('title', 'Update')
@section('content')

        <div class="container">
            <div class="row form-only" >
                <form  method="get" action="{{ route('update') }}">
                    @csrf
                
                    <fieldset>
                            
                        <!-- Form Name -->
                        <legend>Atualizar o Cadastro de usuário</legend>
                        <div class="row">
                            
                            <!-- Text input-->
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label" for="name">Nome</label>
                                    <input id="name" name="nome" type="text" placeholder="Nome Completo" value="" class="form-control input-md">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="row">
                                <div class="form-group col">
                                    <label class="control-label" for="cpf">CPF</label>  
                                    <div class="">
                                        <input id="cpf" name="cpf" type="number" placeholder="CPF" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="control-label" for="cpf">RG</label>  
                                    <div class="">
                                        <input id="rg" name="rg" type="number" placeholder="RG" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col ">
                                    <label class="control-label form-check form-check-inline" for="rad">Sexo</label>
                                    <div class="">
                                        <div class="radio form-check form-check-inline">
                                            <label for="rad-0">
                                            <input type="radio" name="genero" id="rad-0" value="1" checked="checked">
                                            Masculino
                                            </label>
                                        </div>
                                        <div class="radio form-check form-check-inline">
                                            <label for="rad-1">
                                            <input type="radio" name="genero" id="rad-1" value="2">
                                            Feminino
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                              <!-- Confirm email-->
                              <div class="row">
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="email">Email</label>
                                    <div class="">
                                        <input id="email" name="email" type="email" placeholder="Email" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="email">Email</label>
                                    <div class="">
                                        <input id="email" name="email" type="email" placeholder="Confirme seu Email" class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Confirm email-->
                            <div class="row">
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="login">Login</label>
                                    <div class="">
                                        <input id="registration" name="login" type="text" placeholder="Criar Login" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="login">Login</label>
                                    <div class="">
                                        <input id="registration" name="login" type="email" placeholder="Confirme seu Login" class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                        
                            <!-- Password input-->
                            <div class="row">
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="password">Senha</label>
                                    <div class="">
                                        <input id="password" name="senha" type="password" placeholder="Senha" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="col-md-4 control-label" for="confirmpassword">Senha</label>
                                    <div class="">
                                        <input id="password_confirmation" name="ComfirmaSenha" type="password" placeholder="Confirme sua senha" class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="row">
                                <div class="form-group col">
                                    <label class="control-label" for="rua">Rua</label>  
                                    <div class="">
                                        <input id="street" name="rua" type="text" placeholder="Rua" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="control-label" for="numero">Número</label>  
                                    <div class="">
                                        <input id="number" name="numero" type="number" placeholder="Numero" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="control-label" for="cep">CEP</label>  
                                    <div class="">
                                        <input id="cep" name="cep" type="text" placeholder="Cep" class="form-control input-md">
                                    </div>
                                </div>
                                
                            </div>

                            <!-- Text input-->
                            <div class="row">
                                <div class="form-group col">
                                    <label class="control-label" for="cidade">Cidade</label>  
                                    <div class="">
                                        <input id="city" name="cidade" type="text" placeholder="Cidade" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="control-label" for="bairro">Bairro</label>  
                                    <div class="">
                                        <input id="neighborhood" name="bairro" type="text" placeholder="Bairro" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label class="control-label" for="estado">Estado</label>  
                                    <div class="">
                                        <input id="state" name="estado" type="text" placeholder="Estado" class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Cadastrar"></label>
                                <div class="col-md-8">
                                    <button id="Cadastrar" name="Cadastrar" class="btn btn-success">Atualizar</button>
                                </div>
                            </div>
                
                        </div>
                    </fieldset>
                </form>
                        
            </div>
        </div>

@endsection
