<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CreateController extends Controller
{

    public function form(Request $request)
    {
        session_start();

        if(!isset($_SESSION['tokenAtual'])) {
            return view('formlogin');

        } else {
            return view('form');
        }
 
    }


    public function create(Request $request)
    {
        session_start();

        if(!isset($_SESSION['tokenAtual'])) {
            return view('formlogin');
        }

        $curl = curl_init();

        $nome = $request->input('nome');
        $login = $request->input('login');
        $email = $request->input('email');
        $senha = $request->input('senha');
        $ComfirmaSenha = $request->input('ComfirmaSenha');
        $cpf = $request->input('cpf');
        $rg = $request->input('rg');
        $genero = $request->input('genero');
        $aniversario = $request->input('aniversario');
        $celular = $request->input('celular');

        $rua = $request->input('rua');
        $numero = $request->input('numero');
        $cep = $request->input('cep');
        $cidade = $request->input('cidade');
        $bairro = $request->input('bairro');
        $estado = $request->input('estado');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://memori.homologacao.net.br/api/auth/client/create',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{ 
        "name" : "'. $nome .'",
        "email" : "'. $email .'",
        "password" : "'. $senha .'",
        "password_confirmation" : "'. $ComfirmaSenha .'",
        "registration" : "55555555555",
        "cpf" : "'. $cpf .'",
        "rg" : "'. $rg .'",
        "gender" : "'. $genero .'",
        "birthdate" : "09/07/2020",
        "validate_start_date" : "20/01/2021",
        "validate_end_date" : null,
        "phone" : null,
        "cellular" : "'. $celular .'",
        "commercial" : null,
        "address" : {
            "cep" : "'. $cep .'",
            "street" : "'. $rua .'",
            "number" : "'. $numero .'",
            "neighborhood" : "'. $bairro .'",
            "city" : "'. $cidade .'",
            "state" : "'. $estado .'",
            "complement" : null
        },
        "plans" : {
            "details" : [
                {
                    "description" : "fgsnjfdm",
                    "type" : "cpf",
                    "amount" : 65.66
                }
            ],
            "salesman" : "HubFintech"
        }
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '. $_SESSION['tokenAtual'],
            'Content-Type: application/json',
            'Cookie: __cfduid=d64c33874fa137ba124f1171469e8cb301618347647'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        dd($response);

        return view('create');
    }


   public function show() {

        $curl = curl_init();

        session_start();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://memori.homologacao.net.br/api/auth/client/listAll',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '. $_SESSION['tokenAtual'],
                'Content-Type: application/json',
                'Cookie: __cfduid=d64c33874fa137ba124f1171469e8cb301618347647'
            ),
        ));

        $response = curl_exec($curl);
        
        curl_close($curl);

        return view('listall', $response);
    }
   



    public function update(Request $request, $id) {
        session_start();

        if(!isset($_SESSION['tokenAtual'])) {
            return view('formlogin');
        }

        $curl = curl_init();

        $nome = $request->input('nome');
        $login = $request->input('login');
        $email = $request->input('email');
        $senha = $request->input('senha');
        $ComfirmaSenha = $request->input('ComfirmaSenha');
        $cpf = $request->input('cpf');
        $rg = $request->input('rg');
        $genero = $request->input('genero');
        $aniversario = $request->input('aniversario');
        $celular = $request->input('celular');

        $rua = $request->input('rua');
        $numero = $request->input('numero');
        $cep = $request->input('cep');
        $cidade = $request->input('cidade');
        $bairro = $request->input('bairro');
        $estado = $request->input('estado');

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://memori.homologacao.net.br/api/auth/client/'. $id .'/update',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS =>'{
          "name" : "sthghsfjsf",
          "email" : "eduardomariano@nucleus.eti.br",
          "password" : "secret",
          "password_confirmation" : "secret",
          "registration" : "55555555555",
          "cpf" : "55555555555",
          "rg" : "476746576457567",
          "gender" : "M",
          "birthdate" : "09/07/2020",
          "validate_start_date" : "20/01/2021",
          "validate_end_date" : null,
          "phone" : null,
          "cellular" : "85990908909",
          "commercial" : null,
          "address" : {
            "cep" : "61940500",
            "street" : "Rua Treze de Maio",
            "number" : "44",
            "neighborhood" : "Guabiraba",
            "city" : "Maranguape",
            "state" : "CE",
            "complement" : null
          },
          "plans" : {
            "details" : [
                {
                    "description" : "fgsnjfdm",
                    "type" : "cpf",
                    "amount" : 69.66
                }
            ],
            "salesman" : "HubFintech"
          }
        }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '. $_SESSION['tokenAtual'],
                'Content-Type: application/json',
                'Cookie: __cfduid=d64c33874fa137ba124f1171469e8cb301618347647'
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;

        return view('update');
    }

    public function formupdate($id)
    {
        session_start();

        if(!isset($_SESSION['tokenAtual'])) {
            return view('login');
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://memori.homologacao.net.br/api/auth/client/'. $id .'/list',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '. $_SESSION['tokenAtual'],
            'Content-Type: application/json',
            'Cookie: __cfduid=d64c33874fa137ba124f1171469e8cb301618347647'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        echo $response;



        return view('update', $response);
    }

}



/*      $nome = $request->input('nome');
        $login = $request->input('login');
        $email = $request->input('email');
        $senha = $request->input('senha');
        $ComfirmaSenha = $request->input('ComfirmaSenha');
        $cpf = $request->input('cpf');
        $rg = $request->input('rg');
        $genero = $request->input('genero');
        $aniversario = $request->input('aniversario');
        $celular = $request->input('celular');

        $rua = $request->input('rua');
        $numero = $request->input('numero');
        $cep= $request->input('cep');
        $cidade = $request->input('cidade');
        $bairro = $request->input('bairro');
        $estado = $request->input('estado');

    

        $response = Http::post('https://memori.homologacao.net.br/api/auth/client/create', [
            'name' => $nome,
            'email' => $email,
            'password' => $senha,
            'password_confirmation' => $ComfirmaSenha,
            'registration' => $login,
            'cpf' => $cpf,
            'rg' => $rg,
            'gender' => $genero,
            'birthdate' => $aniversario,
            'validate_start_date' => '20/01/2021',
            'validate_end_date' => NULL,
            'phone' => NULL,
            'cellular' => $celular,
            'commercial' => NULL,
            'address' => 
                [
                    'cep' => $cep,
                    'street' => $rua,
                    'number' => $numero,
                    'neighborhood' => $bairro,
                    'city' => $cidade,
                    'state' => $estado,
                    'complement' => NULL,
                ], 
            'plans' => 
                [
                'details' => 
                    [
                        0 => 
                        [
                            'description' => 'fgsnjfdm',
                            'type' => 'cpf',
                            'amount' => 65.66,
                        ],
                    ],
                'salesman' => 'HubFintech',
            ],
        ]); 
         
        $dados = $response->json(); */



          /*   $response = Http::get('https://memori.homologacao.net.br/api/auth/client/listAll');
   
    $dados = $response->json(); */