<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
    
class AuthController extends Controller
{
    public function dashboard() {
        session_start();

        if(isset($_SESSION['tokenAtual'])) {
            return view('dashboard');
        }
        return redirect()->route('formlogin');
    }   
   
    public function showLoginForm() {

        return view('formlogin');
    }
    
    public function login(Request $request) {

        $email = ($request->input('email'));
        $senha = ($request->input('senha'));

        $response= Http::post('https://memori.homologacao.net.br/api/auth/login', [
            'email' => $email,
            'password' => $senha
            ]);

        $dados = $response->json();
        
        session_start();   

        if(isset($dados['account'])) {
            $_SESSION['tokenAtual'] = $dados['account']['token'];
            
            
            return view('dashboard', compact('dados'));
        }

        return redirect()->back()->withInput()->withErrors(['Erro! Os dados não conferem']);

    }

    public function logout() {
        session_start(); 
        
        unset($_SESSION['tokenAtual']);
        return redirect()->route('formlogin'); 

    }
    

}
    